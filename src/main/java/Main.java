import javax.persistence.Persistence;

public class Main {

    public static void main(String[] args) {

        var emf = Persistence.createEntityManagerFactory("mysql");
        var em = emf.createEntityManager();

        System.out.println("Conexao aberta");

        em.close();
        emf.close();
    }
}
